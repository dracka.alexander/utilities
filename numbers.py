#!/usr/bin/env python3
import unittest


def isprime(number):
    if number == 1:
        return False
    for i in range(2, number - 1):
        if number % i == 0:
            return False
    return True


def isprime_shorter(number):
    print(not any(x for x in range(2, number - 1) if number % x == 0))


class TestPrimeNumberFunction(unittest.TestCase):
    """Basic tests for prime numbers."""
    def test_is_number_one_prime_false(self):
        expected = False
        actual = isprime(1)
        self.assertEqual(actual, expected)
        print('Success: One is not a prime number')

    def test_is_number_two_prime_true(self):
        expected = True
        actual = isprime(2)
        self.assertEqual(actual, expected)
        print('Success: Two is a prime number')

    def test_is_number_five_prime_true(self):
        expected = True
        actual = isprime(5)
        self.assertEqual(actual, expected)
        print('Success: Five is a prime number')

    def test_is_number_nine_prime_false(self):
        expected = False
        actual = isprime(9)
        self.assertEqual(actual, expected)
        print('Success: Nine is not a prime number')
