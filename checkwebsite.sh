#!/bin/bash
SITES="https://dracka.net,https://svatba.dracka.net"
EMAILS="dracka.alexander@protonmail.com"
  for SITE in $(echo $SITES | tr "," " "); do
  if [ ! -z "${SITE}" ]; then
    RESPONSE=$(curl -s --head $SITE)
    if echo $RESPONSE | grep "200 OK" > /dev/null
    then
      echo "The HTTP Server on ${SITE} is up!"
    else
      MESSAGE="The HTTP server at ${SITE} has failed to respond."
      for EMAIL in $(echo $EMAILS | tr "," " "); do
        SUBJECT="${SITE} (http) Failed"
        echo $MESSAGE | mail -s "$SUBJECT" $EMAIL
        echo $SUBJECT
        echo "Alert sent to $EMAIL"
      done
    fi
  fi
done